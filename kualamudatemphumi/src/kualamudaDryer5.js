const sensor1 = {
	ref: 1,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor2 = {
	ref: 2,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor3 = {
	ref: 3,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor4 = {
	ref: 4,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor5 = {
	ref: 5,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor6 = {
	ref: 6,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor7 = {
	ref: 7,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor8 = {
	ref: 8,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor9 = {
	ref: 9,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor10 = {
	ref: 10,
	uuid: '11ecf397-023e-4cfa-a3f1-7a39e463cc69',
};
const sensor11 = {
	ref: 11,
	uuid: '9793f7f9-14bf-47cd-b42d-6f4fee6a3642',
};
const sensor12 = {
	ref: 12,
	uuid: '9793f7f9-14bf-47cd-b42d-6f4fee6a3642',
};
const sensor13 = {
	ref: 13,
	uuid: '9793f7f9-14bf-47cd-b42d-6f4fee6a3642',
};
const sensor14 = {
	ref: 14,
	uuid: '9793f7f9-14bf-47cd-b42d-6f4fee6a3642',
};
const sensor15 = {
	ref: 15,
	uuid: '9793f7f9-14bf-47cd-b42d-6f4fee6a3642',
};
const sensor16 = {
	ref: 16,
	uuid: '9793f7f9-14bf-47cd-b42d-6f4fee6a3642',
};
const sensor17 = {
	ref: 17,
	uuid: '9793f7f9-14bf-47cd-b42d-6f4fee6a3642',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

export const profileSensor1 = device;
