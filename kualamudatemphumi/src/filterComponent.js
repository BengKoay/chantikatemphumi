import React, { useState, useEffect } from 'react';
import { Alert, Row, Col, Button, Form } from 'react-bootstrap';
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";

const filterComponent = () => {

  return (
    <>
      <h4>Filter </h4>
      <Row style={{ marginBottom: '20px' }}>
        <Col xs={12} md={6}>
          <Form.Label>From Date</Form.Label>
          <Datetime input={false}></Datetime>
        </Col>
        <Col xs={6} md={6}>
          <Form.Label>To Date</Form.Label>

        </Col>
      </Row>
    </>
  )
}

export default filterComponent;
