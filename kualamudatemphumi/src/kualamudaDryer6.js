const sensor1 = {
	ref: 1,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor2 = {
	ref: 2,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor3 = {
	ref: 3,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor4 = {
	ref: 4,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor5 = {
	ref: 5,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor6 = {
	ref: 6,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor7 = {
	ref: 7,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor8 = {
	ref: 8,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor9 = {
	ref: 9,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor10 = {
	ref: 10,
	uuid: '9ff41e9c-9a43-4834-b8c6-3292f497792f',
};
const sensor11 = {
	ref: 11,
	uuid: '2d4df282-9828-4c1b-80fc-ea175bb00993',
};
const sensor12 = {
	ref: 12,
	uuid: '2d4df282-9828-4c1b-80fc-ea175bb00993',
};
const sensor13 = {
	ref: 13,
	uuid: '2d4df282-9828-4c1b-80fc-ea175bb00993',
};
const sensor14 = {
	ref: 14,
	uuid: '2d4df282-9828-4c1b-80fc-ea175bb00993',
};
const sensor15 = {
	ref: 15,
	uuid: '2d4df282-9828-4c1b-80fc-ea175bb00993',
};
const sensor16 = {
	ref: 16,
	uuid: '2d4df282-9828-4c1b-80fc-ea175bb00993',
};
const sensor17 = {
	ref: 17,
	uuid: '2d4df282-9828-4c1b-80fc-ea175bb00993',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

export const profileSensor1 = device;
