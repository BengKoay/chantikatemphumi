import moment from 'moment';
import axios from 'axios';
import aws4Interceptor from 'aws4-axios';
import React, { useState, useEffect } from 'react';
import LineChart from './lineChart';
import FilterComponent from './filterComponent';
import { Container } from 'react-bootstrap';

import { profileSensorA11 } from './chantikaDryerA11';
import { profileSensorA13 } from './chantikaDryerA13';
import { profileSensorA5 } from './chantikaDryerA5';

const color = [
	{ ref: 1, color: '#d84315' },
	{ ref: 2, color: '#ff5722' },
	{ ref: 3, color: '#ffab91' },
	{ ref: 4, color: '#2e7d32' },
	{ ref: 5, color: '#4caf50' },
	{ ref: 6, color: '#aed581' },
	{ ref: 7, color: '#1565c0' },
	{ ref: 8, color: '#2196f3' },
	{ ref: 9, color: '#b3e5fc' },
	{ ref: 10, color: '#512da8' },
	{ ref: 11, color: '#9c27b0' },
	{ ref: 12, color: '#ce93d8' },
	{ ref: 13, color: '#424242' },
	{ ref: 14, color: '#9e9e9e' },
	{ ref: 15, color: '#9480E2' },
	{ ref: 16, color: '#7561C6' },
	{ ref: 17, color: '#000000' },
];

// const axiosSolar = {
// 	baseURL: 'https://9ry30zin92.execute-api.ap-southeast-1.amazonaws.com/public',
// 	apiKey: 'eJzWdiiQi18jzBIWxqFHV1TRAgfcIg8h8Lc0wgBB',
// 	region: 'ap-southeast-1',
// 	service: 'execute-api',
// 	accessKeyId: 'AKIAVBSYQGXZE6POVWVK',
// 	secretAccessKey: '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',
// };
// const axiosMainHelper = axios.create({
// 	baseURL: axiosSolar.baseURL,
// 	headers: { 'x-api-key': axiosSolar.apiKey },
// });
// const interceptor = aws4Interceptor(
// 	{
// 		region: axiosSolar.region,
// 		service: axiosSolar.service,
// 	},
// 	{
// 		accessKeyId: axiosSolar.accessKeyId,
// 		secretAccessKey: axiosSolar.secretAccessKey,
// 	}
// );

// axiosMainHelper.interceptors.request.use(interceptor);

const axiosSolar = {
	baseURL: 'https://5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com',
	authorization: 'a414f268-5ab2-4854-b4f7-97a9ea6d6fec',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: {
		Authorization: axiosSolar.authorization,
		// 'Access-Control-Allow-Origin': '*',
		// 'Access-Control-Allow-Headers': '*',
		// 'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
	},
});

const DynamicChart = () => {
	const [graphTemperatureA5, setGraphTemperatureA5] = useState([]);
	const [graphTemperatureA11, setGraphTemperatureA11] = useState([]);
	const [graphTemperatureA13, setGraphTemperatureA13] = useState([]);
	const [graphCO2, setGraphCO2] = useState([]);
	const [graphPMSPUG25, setGraphPMSPUG25] = useState([]);

	const startDate = moment.utc().subtract(2, 'day').unix(); // moment().startOf('day').unix() * 1000
	const dateNow = moment().unix();

	// const startDate = 1639589494; // moment().startOf('day').unix() * 1000
	// const dateNow = 1639972906;

	const getData = (deviceInfo, startDate, endDate) => {
		return new Promise(async (resolve, reject) => {
			const data = { ref: deviceInfo.ref, data: [] };
			axiosMainHelper
				.get(
					// `/rawtmpdata/?deviceID=${deviceInfo.uuidSource}&start_time=${startDate}&end_time=${endDate}`
					`devicePayload?deviceId=${deviceInfo.uuid}&ref=${deviceInfo.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
				)
				.then((res) => {
					console.log('res', res);
					for (const eachDataPoint of res.data.data) {
						try {
							const info = {};
							info.ref = deviceInfo.ref;
							// console.log('eachDataPoint', eachDataPoint);
							// let eachDataPoint = JSON.parse(
							// 	res.data[`${row}`].stringifiedData.S
							// );
							if (eachDataPoint.hasOwnProperty('timestamp')) {
								info.timestamp = eachDataPoint.timestamp;
							}
							if (eachDataPoint.hasOwnProperty('temperature')) {
								info.temperature = eachDataPoint.temperature;
							}
							if (eachDataPoint.hasOwnProperty('humidity')) {
								info.humidity = eachDataPoint.humidity;
							}
							if (eachDataPoint.hasOwnProperty('co2')) {
								info.co2 = eachDataPoint.co2;
							}
							if (
								!!eachDataPoint.AQI &&
								eachDataPoint.AQI.hasOwnProperty('PM_SP_UG_2_5')
							) {
								info.PM_SP_UG_2_5 = eachDataPoint.AQI.PM_SP_UG_2_5;
							}
							data.data.push(info);
						} catch (e) {
							console.log(e);
						}
					}
					resolve(data);
				});
		});
	};

	const loadDataA5 = async (profileSensor, startDate, endDate) => {
		await Promise.all(
			profileSensor.map((item) => {
				return getData(item, startDate, endDate);
			})
		).then((result) => {
			// console.log(result, 'result');
			const infoTemperature = [];
			const infoCO2 = [];
			const infoPMSPUG = [];
			//process data in evey 5 minutes
			let startMinutes = moment(startDate).get('minutes') % 5;
			let endMinutes = 5 - (moment(endDate).get('minutes') % 5);
			let startProcess = moment(startDate * 1000).subtract(
				startMinutes,
				'minutes'
			);
			let endProcess = moment(endDate * 1000).add(endMinutes, 'minutes');
			//console.log(startDate, moment(startDate).subtract(startMinutes, "minutes"), startMinutes, endDate, moment(endDate).add(endMinutes, "minutes"), endMinutes)
			let index = 0;
			console.log(
				'startMinutes',
				typeof startDate,
				startMinutes,
				startProcess.format('YYYY-MM-DD HH:mm'),
				startProcess.unix()
			);
			console.log(
				'endMinutes',
				typeof endDate,
				endMinutes,
				endProcess.format('YYYY-MM-DD HH:mm'),
				endProcess.unix()
			);
			// console.log(
			// 	'result[0].data[0].timestamp',
			// 	moment(result[0].data[0].timestamp).unix()
			// );
			while (startProcess.unix() < endProcess.unix()) {
				// console.log(startProcess.format('YYYY-MM-DD HH:mm:ss'));
				let nextProcess = moment(startProcess).add(5, 'minutes');

				const dataTemperature = { time: startProcess.format('HH:mm') };
				const dataCO2 = { time: startProcess.format('HH:mm') };
				const dataPMSPUG = { time: startProcess.format('HH:mm') };

				result.map((item) => {
					// console.log('item', item);
					let dataPoint = item.data.find(
						(obj) =>
							moment(obj.timestamp).unix() >= startProcess.unix() &&
							moment(obj.timestamp).unix() < nextProcess.unix()
					);
					if (!!dataPoint) {
						if (!!dataPoint.temperature) {
							dataTemperature[`${dataPoint.ref}_temperature`] =
								dataPoint.temperature;
							// console.log('dataPoint', dataPoint);
						}
						if (!!dataPoint.humidity) {
							dataTemperature[`${dataPoint.ref}_humidity`] = dataPoint.humidity;
						}
						if (!!dataPoint.co2) {
							dataCO2[`${dataPoint.ref}`] = dataPoint.co2;
						}
						if (!!dataPoint.PM_SP_UG_2_5) {
							dataPMSPUG[`${dataPoint.ref}`] = dataPoint.PM_SP_UG_2_5;
						}
					} else {
						// if (index - 1 != -1) {
						//   dataPoint = infoTemperature[index - 1];
						//   if (!!dataPoint[`${item.name}_temperature`]) {
						//     dataTemperature[`${item.name}_temperature`] = dataPoint[`${item.name}_temperature`]
						//   }
						//   if (!!dataPoint[`${item.name}_humidity`]) {
						//     dataTemperature[`${item.name}_humidity`] = dataPoint[`${item.name}_humidity`]
						//   }
						//   if (!!dataPoint[`${item.name}`]) {
						//     dataCO2[`${item.name}`] = dataPoint[`${item.name}`]
						//   }
						//   if (!!dataPoint[`${item.name}`]) {
						//     dataPMSPUG[`${item.name}`] = dataPoint[`${item.name}`]
						//   }
						//   //console.log(infoTemperature[index - 1], item.name, dataPoint, dataTemperature)
						// }
					}
					//console.log(dataPoint, startProcess.unix(), nextProcess.unix());
				});

				index += 1;
				infoTemperature.push(dataTemperature);
				infoCO2.push(dataCO2);
				infoPMSPUG.push(dataPMSPUG);
				startProcess = nextProcess;
			}
			// console.log('infoTemperature', infoTemperature);
			setGraphTemperatureA5({ colorCode: color, dataSet: infoTemperature });
			setGraphCO2({ colorCode: color, dataSet: infoCO2 });
			setGraphPMSPUG25({ colorCode: color, dataSet: infoPMSPUG });
		});
	};
	const loadDataA11 = async (profileSensor, startDate, endDate) => {
		await Promise.all(
			profileSensor.map((item) => {
				return getData(item, startDate, endDate);
			})
		).then((result) => {
			// console.log(result, 'result');
			const infoTemperature = [];
			const infoCO2 = [];
			const infoPMSPUG = [];
			//process data in evey 5 minutes
			let startMinutes = moment(startDate).get('minutes') % 5;
			let endMinutes = 5 - (moment(endDate).get('minutes') % 5);
			let startProcess = moment(startDate * 1000).subtract(
				startMinutes,
				'minutes'
			);
			let endProcess = moment(endDate * 1000).add(endMinutes, 'minutes');
			//console.log(startDate, moment(startDate).subtract(startMinutes, "minutes"), startMinutes, endDate, moment(endDate).add(endMinutes, "minutes"), endMinutes)
			let index = 0;
			console.log(
				'startMinutes',
				typeof startDate,
				startMinutes,
				startProcess.format('YYYY-MM-DD HH:mm'),
				startProcess.unix()
			);
			console.log(
				'endMinutes',
				typeof endDate,
				endMinutes,
				endProcess.format('YYYY-MM-DD HH:mm'),
				endProcess.unix()
			);
			// console.log(
			// 	'result[0].data[0].timestamp',
			// 	moment(result[0].data[0].timestamp).unix()
			// );
			while (startProcess.unix() < endProcess.unix()) {
				// console.log(startProcess.format('YYYY-MM-DD HH:mm:ss'));
				let nextProcess = moment(startProcess).add(5, 'minutes');

				const dataTemperature = { time: startProcess.format('HH:mm') };
				const dataCO2 = { time: startProcess.format('HH:mm') };
				const dataPMSPUG = { time: startProcess.format('HH:mm') };

				result.map((item) => {
					// console.log('item', item);
					let dataPoint = item.data.find(
						(obj) =>
							moment(obj.timestamp).unix() >= startProcess.unix() &&
							moment(obj.timestamp).unix() < nextProcess.unix()
					);
					if (!!dataPoint) {
						if (!!dataPoint.temperature) {
							dataTemperature[`${dataPoint.ref}_temperature`] =
								dataPoint.temperature;
							// console.log('dataPoint', dataPoint);
						}
						if (!!dataPoint.humidity) {
							dataTemperature[`${dataPoint.ref}_humidity`] = dataPoint.humidity;
						}
						if (!!dataPoint.co2) {
							dataCO2[`${dataPoint.ref}`] = dataPoint.co2;
						}
						if (!!dataPoint.PM_SP_UG_2_5) {
							dataPMSPUG[`${dataPoint.ref}`] = dataPoint.PM_SP_UG_2_5;
						}
					} else {
						// if (index - 1 != -1) {
						//   dataPoint = infoTemperature[index - 1];
						//   if (!!dataPoint[`${item.name}_temperature`]) {
						//     dataTemperature[`${item.name}_temperature`] = dataPoint[`${item.name}_temperature`]
						//   }
						//   if (!!dataPoint[`${item.name}_humidity`]) {
						//     dataTemperature[`${item.name}_humidity`] = dataPoint[`${item.name}_humidity`]
						//   }
						//   if (!!dataPoint[`${item.name}`]) {
						//     dataCO2[`${item.name}`] = dataPoint[`${item.name}`]
						//   }
						//   if (!!dataPoint[`${item.name}`]) {
						//     dataPMSPUG[`${item.name}`] = dataPoint[`${item.name}`]
						//   }
						//   //console.log(infoTemperature[index - 1], item.name, dataPoint, dataTemperature)
						// }
					}
					//console.log(dataPoint, startProcess.unix(), nextProcess.unix());
				});

				index += 1;
				infoTemperature.push(dataTemperature);
				infoCO2.push(dataCO2);
				infoPMSPUG.push(dataPMSPUG);
				startProcess = nextProcess;
			}
			// console.log('infoTemperature', infoTemperature);
			setGraphTemperatureA11({ colorCode: color, dataSet: infoTemperature });
			setGraphCO2({ colorCode: color, dataSet: infoCO2 });
			setGraphPMSPUG25({ colorCode: color, dataSet: infoPMSPUG });
		});
	};
	const loadDataA13 = async (profileSensor, startDate, endDate) => {
		await Promise.all(
			profileSensor.map((item) => {
				return getData(item, startDate, endDate);
			})
		).then((result) => {
			// console.log(result, 'result');
			const infoTemperature = [];
			const infoCO2 = [];
			const infoPMSPUG = [];
			//process data in evey 5 minutes
			let startMinutes = moment(startDate).get('minutes') % 5;
			let endMinutes = 5 - (moment(endDate).get('minutes') % 5);
			let startProcess = moment(startDate * 1000).subtract(
				startMinutes,
				'minutes'
			);
			let endProcess = moment(endDate * 1000).add(endMinutes, 'minutes');
			//console.log(startDate, moment(startDate).subtract(startMinutes, "minutes"), startMinutes, endDate, moment(endDate).add(endMinutes, "minutes"), endMinutes)
			let index = 0;
			console.log(
				'startMinutes',
				typeof startDate,
				startMinutes,
				startProcess.format('YYYY-MM-DD HH:mm'),
				startProcess.unix()
			);
			console.log(
				'endMinutes',
				typeof endDate,
				endMinutes,
				endProcess.format('YYYY-MM-DD HH:mm'),
				endProcess.unix()
			);
			// console.log(
			// 	'result[0].data[0].timestamp',
			// 	moment(result[0].data[0].timestamp).unix()
			// );
			while (startProcess.unix() < endProcess.unix()) {
				// console.log(startProcess.format('YYYY-MM-DD HH:mm:ss'));
				let nextProcess = moment(startProcess).add(5, 'minutes');

				const dataTemperature = { time: startProcess.format('HH:mm') };
				const dataCO2 = { time: startProcess.format('HH:mm') };
				const dataPMSPUG = { time: startProcess.format('HH:mm') };

				result.map((item) => {
					// console.log('item', item);
					let dataPoint = item.data.find(
						(obj) =>
							moment(obj.timestamp).unix() >= startProcess.unix() &&
							moment(obj.timestamp).unix() < nextProcess.unix()
					);
					if (!!dataPoint) {
						if (!!dataPoint.temperature) {
							dataTemperature[`${dataPoint.ref}_temperature`] =
								dataPoint.temperature;
							// console.log('dataPoint', dataPoint);
						}
						if (!!dataPoint.humidity) {
							dataTemperature[`${dataPoint.ref}_humidity`] = dataPoint.humidity;
						}
						if (!!dataPoint.co2) {
							dataCO2[`${dataPoint.ref}`] = dataPoint.co2;
						}
						if (!!dataPoint.PM_SP_UG_2_5) {
							dataPMSPUG[`${dataPoint.ref}`] = dataPoint.PM_SP_UG_2_5;
						}
					} else {
						// if (index - 1 != -1) {
						//   dataPoint = infoTemperature[index - 1];
						//   if (!!dataPoint[`${item.name}_temperature`]) {
						//     dataTemperature[`${item.name}_temperature`] = dataPoint[`${item.name}_temperature`]
						//   }
						//   if (!!dataPoint[`${item.name}_humidity`]) {
						//     dataTemperature[`${item.name}_humidity`] = dataPoint[`${item.name}_humidity`]
						//   }
						//   if (!!dataPoint[`${item.name}`]) {
						//     dataCO2[`${item.name}`] = dataPoint[`${item.name}`]
						//   }
						//   if (!!dataPoint[`${item.name}`]) {
						//     dataPMSPUG[`${item.name}`] = dataPoint[`${item.name}`]
						//   }
						//   //console.log(infoTemperature[index - 1], item.name, dataPoint, dataTemperature)
						// }
					}
					//console.log(dataPoint, startProcess.unix(), nextProcess.unix());
				});

				index += 1;
				infoTemperature.push(dataTemperature);
				infoCO2.push(dataCO2);
				infoPMSPUG.push(dataPMSPUG);
				startProcess = nextProcess;
			}
			// console.log('infoTemperature', infoTemperature);
			setGraphTemperatureA13({ colorCode: color, dataSet: infoTemperature });
			setGraphCO2({ colorCode: color, dataSet: infoCO2 });
			setGraphPMSPUG25({ colorCode: color, dataSet: infoPMSPUG });
		});
	};

	useEffect(() => {
		// loadData(profileSensor2, startDate, dateNow);
		// loadDataA5(profileSensorA5, startDate, dateNow);
		loadDataA11(profileSensorA11, startDate, dateNow);
		// loadDataA13(profileSensorA13, startDate, dateNow);
	}, []);

	return (
		<Container className="App">
			{/* <FilterComponent /> */}
			{/* <LineChart
				title={'HumidityTemperature A5'}
				type={'temperature'}
				dataSet={graphTemperatureA5}
				device={profileSensorA5}
			/> */}
			<LineChart
				title={'HumidityTemperature A11'}
				type={'temperature'}
				dataSet={graphTemperatureA11}
				device={profileSensorA11}
			/>
			{/* <LineChart
				title={'HumidityTemperature A13'}
				type={'temperature'}
				dataSet={graphTemperatureA13}
				device={profileSensorA13}
			/> */}
			{/* <LineChart
				title={'CO2'}
				type={'co2'}
				dataSet={graphCO2}
				device={device}
			/>
			<LineChart
				title={'PM_SP_UG_2_5'}
				type={'PMSPUG25'}
				dataSet={graphPMSPUG25}
				device={device}
			/> */}
		</Container>
	);
};

export default DynamicChart;
