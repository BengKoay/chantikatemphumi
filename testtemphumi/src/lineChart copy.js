import React, { useState, useEffect, useRef } from 'react';
import {
	ResponsiveContainer,
	LineChart,
	Line,
	Legend,
	Tooltip,
	CartesianGrid,
	XAxis,
	YAxis,
} from 'recharts';
import './App.css';

const LineChartComponent = (props) => {
	const { title = '', type, dataSet, device } = props;
	// console.log('type', type, 'dataSet', dataSet, 'device', device);

	let screenSize = { width: 0, height: 400 };
	const [chartSize, setChartSize] = useState({ width: 400, height: 400 });
	const targetRef = useRef();

	useEffect(() => {
		if (targetRef.current) {
			setChartSize({ ...chartSize, width: targetRef.current.offsetWidth });
		}
	}, []);

	const legendIndicator = (props) => {
		const { height, width, color } = props;
		return (
			<div
				style={{
					backgroundColor: color,
					height: height,
					width: width,
					display: 'inline-block',
					marginRight: '5px',
					borderRadius: '50%',
				}}
			></div>
		);
	};

	const generateRandomColor = () => {
		const letters = '0123456789ABCDEF';
		let color = '#';
		for (let i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	};

	const renderLegend = (props) => {
		const { payload } = props;
		const info = [];

		switch (type) {
			case 'co2':
				device
					.filter((item) => item.co2 === true)
					.map((sensor, index) => {
						const itemIndex = info.findIndex(
							(item) => item.value == sensor.name
						);
						if (itemIndex == -1) {
							const color = dataSet.colorCode.find(
								(item) => item.name == sensor.name
							);
							info.push({ color: color.color, value: sensor.name });
						}
					});
				break;
			default:
				device.map((sensor, index) => {
					// console.log('sensor', sensor);
					const itemIndex = info.findIndex((item) => item.value == sensor.ref);
					if (itemIndex == -1) {
						const color = dataSet.colorCode.find(
							(item) => item.ref == sensor.ref
						);
						info.push({ color: color.color, value: sensor.ref });
					}
				});
		}

		let content = '';

		content = (
			<div>
				<ul
					className="recharts-default-legend"
					style={{
						padding: '0px',
						margin: '0px',
						textAlign: 'center',
						listStyleType: 'none',
					}}
				>
					{info.map((entry, index) => {
						return (
							<li
								className="recharts-legend-item"
								style={{ display: 'inline-block', marginRight: '10px' }}
								key={`item-${index}`}
							>
								{legendIndicator({ width: 10, height: 10, color: entry.color })}
								{entry.value}
							</li>
						);
					})}
				</ul>
			</div>
		);

		return content;
	};

	const renderTooltip = (props) => {
		const { payload } = props;
		const info = [];
		let label = '';

		payload.map((entry, index) => {
			label = entry.payload.time;
			const itemIndex = info.findIndex((item) => item.color == entry.color);
			if (itemIndex != -1) {
				if (entry.name.split('_')[1] != 'humidity') {
					info[itemIndex].name += '/' + entry.name.split('_')[0];
				}
				info[itemIndex].value += '/' + entry.value.toFixed(2) + ' ºC';
			} else {
				info.push({
					color: entry.color,
					name: entry.name.split('_')[0],
					value: entry.value.toFixed(3) + '%',
				});
			}
		});

		return (
			<>
				<div className="custom-tooltip">
					<div className="intro">Time {label}</div>
					<ul style={{ paddingLeft: 0, listStyleType: 'none' }}>
						{info.map((entry, index) => (
							<li key={`item-${index}`}>
								{legendIndicator({ width: 10, height: 10, color: entry.color })}
								{entry.name} : {entry.value}
							</li>
						))}
					</ul>
				</div>
			</>
		);
	};

	const renderChart = () => {
		if (!dataSet || !dataSet.dataSet || dataSet.dataSet.length === 0) {
			return <div style={{ height: `${chartSize.height} px` }}>Loading...</div>;
		}

		let lineComponents = [];
		if (dataSet.dataSet.length > 0) {
			switch (type) {
				case 'temperature':
					device.map((sensor, index) => {
						const color = dataSet.colorCode.find(
							(item) => item.ref == sensor.ref
						);
						lineComponents.push(
							<Line
								key={`${sensor.ref}_humidity`}
								type="monotone"
								dataKey={`${sensor.ref}_humidity`}
								name={`${sensor.ref}_humidity`}
								stroke={color.color}
								dot={false}
								yAxisId="right"
							/>
						);
						lineComponents.push(
							<Line
								key={`${sensor.ref}_temperature`}
								type="monotone"
								dataKey={`${sensor.ref}_temperature`}
								name={`${sensor.ref}_temperature`}
								stroke={color.color}
								dot={false}
								yAxisId="left"
							/>
						);
					});
					break;
				default:
					device
						.filter((item) => item.co2 === true)
						.map((sensor, index) => {
							const color = dataSet.colorCode.find(
								(item) => item.name == sensor.name
							);
							lineComponents.push(
								<Line
									key={sensor.name}
									type="monotone"
									dataKey={sensor.name}
									name={sensor.name}
									stroke={color.color}
									dot={false}
								/>
							);
						});
			}
		}

		let yAxisComponents = [];
		switch (type) {
			case 'temperature':
				yAxisComponents.push(
					<YAxis
						yAxisId="left"
						connectNulls
						domain={[20, 50]}
						key={`${type}_yaxis_left`}
					/>
				);
				yAxisComponents.push(
					<YAxis
						yAxisId="right"
						orientation="right"
						connectNulls
						key={`${type}_yaxis_right`}
					/>
				);
				break;
			case 'co2':
				yAxisComponents.push(
					<YAxis domain={[350, 450]} key={`${type}_yaxis`} />
				);
				break;
			default:
				yAxisComponents.push(<YAxis key={`${type}_yaxis`} />);
		}

		let tooltipComponents = [];
		switch (type) {
			case 'temperature':
				tooltipComponents.push(
					<Tooltip
						position={{ y: 200 }}
						wrapperStyle={{ zIndex: 1000 }}
						cursor={true}
						content={renderTooltip}
						key={`${type}_tooltip`}
						// coordinate={{ x: 100, y: 140 }}
						// offsetX={100}
					/>
				);
				break;
			default:
				tooltipComponents.push(
					<Tooltip
						cursor={false}
						wrapperStyle={{ zIndex: 1000 }}
						key={`${type}_tooltip`}
					/>
				);
		}

		let legandComponents = [];
		switch (type) {
			case 'temperature':
				legandComponents.push(
					<Legend content={renderLegend} key={`${type}_legand`} />
				);
				break;
			case 'co2':
				legandComponents.push(
					<Legend content={renderLegend} key={`${type}_legand`} />
				);
				break;
			default:
				legandComponents.push(
					<Legend iconType="rect" height={10} key={`${type}_legand`} />
				);
		}

		if (dataSet.dataSet.length > 0) {
			return (
				<div style={{ width: '100%', height: screenSize.height }}>
					<ResponsiveContainer>
						<LineChart data={dataSet.dataSet}>
							<XAxis dataKey="time" />
							{yAxisComponents}
							{tooltipComponents}
							<CartesianGrid strokeDasharray="3 3" />
							{legandComponents}
							{lineComponents}
						</LineChart>
					</ResponsiveContainer>
				</div>
			);
		}
	};

	return (
		<>
			<h1>{title}</h1>
			{renderChart()}
		</>
	);
};

export default LineChartComponent;
