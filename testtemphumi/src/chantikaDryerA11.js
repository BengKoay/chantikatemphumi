const sensor1 = {
	ref: 1,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor2 = {
	ref: 2,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor3 = {
	ref: 3,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor4 = {
	ref: 4,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor5 = {
	ref: 5,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor6 = {
	ref: 6,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor7 = {
	ref: 7,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor8 = {
	ref: 8,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor9 = {
	ref: 9,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor10 = {
	ref: 10,
	uuid: '6bfaf4e8-50e6-4120-9cd9-d2639be9bded',
};
const sensor11 = {
	ref: 11,
	uuid: '2b74dc15-d8bf-467d-b15d-e25c3dff260b',
};
const sensor12 = {
	ref: 12,
	uuid: '2b74dc15-d8bf-467d-b15d-e25c3dff260b',
};
const sensor13 = {
	ref: 13,
	uuid: '2b74dc15-d8bf-467d-b15d-e25c3dff260b',
};
const sensor14 = {
	ref: 14,
	uuid: '2b74dc15-d8bf-467d-b15d-e25c3dff260b',
};
const sensor15 = {
	ref: 15,
	uuid: '2b74dc15-d8bf-467d-b15d-e25c3dff260b',
};
const sensor16 = {
	ref: 16,
	uuid: '2b74dc15-d8bf-467d-b15d-e25c3dff260b',
};
const sensor17 = {
	ref: 17,
	uuid: '2b74dc15-d8bf-467d-b15d-e25c3dff260b',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

export const profileSensorA11 = device;
