const sensor1 = {
	ref: 1,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor2 = {
	ref: 2,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor3 = {
	ref: 3,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor4 = {
	ref: 4,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor5 = {
	ref: 5,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor6 = {
	ref: 6,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor7 = {
	ref: 7,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor8 = {
	ref: 8,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor9 = {
	ref: 9,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor10 = {
	ref: 10,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor11 = {
	ref: 11,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor12 = {
	ref: 12,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor13 = {
	ref: 13,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor14 = {
	ref: 14,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor15 = {
	ref: 15,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor16 = {
	ref: 16,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor17 = {
	ref: 17,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

export const profileSensorA5 = device;
