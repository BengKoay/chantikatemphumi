const sensor1 = {
	ref: 1,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor2 = {
	ref: 2,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor3 = {
	ref: 3,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor4 = {
	ref: 4,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor5 = {
	ref: 5,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor6 = {
	ref: 6,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor7 = {
	ref: 7,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor8 = {
	ref: 8,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor9 = {
	ref: 9,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor10 = {
	ref: 10,
	uuid: '517f1b72-0ea3-4a53-a915-ae8dca1568de',
};
const sensor11 = {
	ref: 11,
	uuid: '9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f',
};
const sensor12 = {
	ref: 12,
	uuid: '9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f',
};
const sensor13 = {
	ref: 13,
	uuid: '9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f',
};
const sensor14 = {
	ref: 14,
	uuid: '9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f',
};
const sensor15 = {
	ref: 15,
	uuid: '9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f',
};
const sensor16 = {
	ref: 16,
	uuid: '9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f',
};
const sensor17 = {
	ref: 17,
	uuid: '9d63bb9a-1f70-49ff-93d0-4c7e3a9df09f',
};

const device = [];
device.push(
	sensor1,
	sensor2,
	sensor3,
	sensor4,
	sensor5,
	sensor6,
	sensor7,
	sensor8,
	sensor9,
	sensor10,
	sensor11,
	sensor12,
	sensor13,
	sensor14,
	sensor15,
	sensor16,
	sensor17
);

export const profileSensorA13 = device;
