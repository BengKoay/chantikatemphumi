const axios = require('axios');
const moment = require('moment');

const XLSX = require('xlsx');

const axiosSolar = {
	baseURL: 'https://5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com/',
	authorization: 'a414f268-5ab2-4854-b4f7-97a9ea6d6fec',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: { Authorization: axiosSolar.authorization },
});

// const deviceId = 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a';
// const ref = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const device = [];
const sensor1 = {
	ref: 1,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor2 = {
	ref: 2,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor3 = {
	ref: 3,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor4 = {
	ref: 4,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor5 = {
	ref: 5,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor6 = {
	ref: 6,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor7 = {
	ref: 7,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor8 = {
	ref: 8,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor9 = {
	ref: 9,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor10 = {
	ref: 10,
	uuid: 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a',
};
const sensor11 = {
	ref: 11,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor12 = {
	ref: 12,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor13 = {
	ref: 13,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor14 = {
	ref: 14,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor15 = {
	ref: 15,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor16 = {
	ref: 16,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
const sensor17 = {
	ref: 17,
	uuid: 'f827b10c-d463-4ae4-b2f1-ea890480b065',
};
device.push(sensor1);
// device.push(sensor2);
// device.push(sensor3);
// device.push(sensor4);
// device.push(sensor5);
// device.push(sensor6);
// device.push(sensor7);
// device.push(sensor8);
// device.push(sensor9);
// device.push(sensor10);
// device.push(sensor11);
// device.push(sensor12);
// device.push(sensor13);
// device.push(sensor14);
// device.push(sensor15);
// device.push(sensor16);
// device.push(sensor17);

const timestampFrom = `${moment().startOf('day').subtract(10, 'days').unix()}`; // start of yesterday
const timestampTo = moment().unix(); // now

const pushRefToDataPoint = [];

const main = async () => {
	// const startDate = moment.utc().subtract(1, 'day').unix(); // moment().startOf('day').unix() * 1000
	const startDate = 1640019969; // moment().startOf('day').unix() * 1000
	const dateNow = moment().unix();
	// const dateNow = 1639972906;
	// const data = {};
	const data = await getData(startDate, dateNow);
	// await writeToExcel(data, startDate, dateNow);

	// console.log('data', data);
	// const infoTemperature = [];
	// // const infoHumidity = [];
	// //process data in evey 5 minutes
	// let startMinutes = moment(startDate).get('minutes') % 5;
	// let endMinutes = 5 - (moment(dateNow).get('minutes') % 5);
	// let startProcess = moment(startDate).subtract(startMinutes, 'minutes');
	// let endProcess = moment(dateNow).add(endMinutes, 'minutes');
	// let index = 0;
	// while (startProcess.unix() < endProcess.unix()) {
	// 	let nextProcess = moment(startProcess).add(5, 'minutes');
	// 	const dataTemperature = { time: startProcess.format('HH:mm') };
	// 	// const dataHumidity = { time: startProcess.format('HH:mm') };
	// 	for (const eachResult of data.data) {
	// 		// console.log(eachResult);
	// 		if (
	// 			eachResult.timestamp >= startProcess.unix() &&
	// 			eachResult.timestamp < nextProcess.unix()
	// 		) {
	// 			if (!!eachResult.temperature) {
	// 				dataTemperature[`${eachResult.ref}_temperature`] =
	// 					eachResult.temperature;
	// 			}
	// 			if (!!eachResult.humidity) {
	// 				dataTemperature[`${eachResult.ref}_humidity`] = eachResult.humidity;
	// 			}
	// 		}
	// 	}
	// 	infoTemperature.push(dataTemperature);
	// }
	// console.log('infoTemperature', infoTemperature);

	const infoTemperature = await testTwo(data, startDate, dateNow);
};

const getData = async (startDate, dateNow) => {
	// console.log('testOne');
	// console.log(startDate);
	// console.log(dateNow);
	const collectedData = [];
	for (const eachDevice of device) {
		const data = {};
		// console.log('eachDevice', eachDevice);
		data.ref = eachDevice.ref;
		data.data = [];
		try {
			const res = await axiosMainHelper.get(
				// `devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
				`devicePayload?deviceId=${eachDevice.uuid}&ref=${eachDevice.ref}&timestampFrom=${startDate}&timestampTo=${dateNow}`
			);
			// console.log(res);
			for (const row of res.data.data) {
				const info = {};
				info.ref = eachDevice.ref;
				// console.log(row);
				// console.log(row.deviceId);
				// console.log(row.ref, row.temperature, row.humidity);
				if (row.hasOwnProperty('timestamp')) {
					info.timestamp = row.timestamp;
				}
				if (row.hasOwnProperty('temperature')) {
					info.temperature = row.temperature;
				}
				if (row.hasOwnProperty('humidity')) {
					info.humidity = row.humidity;
				}
				if (row.hasOwnProperty('deviceId')) {
					info.deviceId = row.deviceId;
				}
				if (row.hasOwnProperty('ref')) {
					info.ref = row.ref;
				}
				// console.log(row.temperature);
				// console.log(row.humidity);
				data.data.push(info);
			}
		} catch (e) {
			console.log('error ', e);
		}
		console.log('data.ref', data.ref, data.data[0]);
		collectedData.push(data);
		// console.log(collectedData);
	}
	// console.log('data', data);
	return collectedData;
};

const writeToExcel = async (data, startDate, dateNow) => {
	let wb = XLSX.utils.book_new();
	let exportFileName = `dryerA5_${startDate}_${dateNow}.xlsx`;
	for (const eachRef of data) {
		// console.log('writeToExcel', eachRef);
		// for each array loop, eachRef
		let ws = XLSX.utils.json_to_sheet(eachRef.data);
		let sheetName = `${eachRef.ref}`;
		console.log('sheetName', sheetName);
		XLSX.utils.book_append_sheet(wb, ws, sheetName);
	}
	XLSX.writeFile(wb, exportFileName);
};
const testTwo = async (data, startDate, dateNow) => {
	console.log('testTwo');
	let startDateMoment = moment().unix(startDate);
	let dateNowMoment = moment().unix(dateNow);
	// console.log('data', data);
	console.log(startDateMoment);
	console.log(dateNowMoment);
	const infoTemperature = [];
	// const infoHumidity = [];
	//process data in evey 5 minutes
	try {
		// let startMoment = moment().unix(startDate);
		// // console.log('startMoment', typeof startMoment);
		// let endMoment = moment().unix(dateNow);

		// get difference between 2 moment in unix

		// while loop difference
		// increment of 1 minute using difference

		const now = moment.unix(startDate); // create a moment with the current time
		const then = dateNow; // create a moment with the other time timestamp in seconds
		const delta = now.diff(then, 'minutes'); // get the millisecond difference

		// console.log(delta);
		let tempDiff = 0;
		while (tempDiff < delta) {
			let startAddTempDiff = now.add(tempDiff, 'minutes');
			let endAddTempDiff = now.add(tempDiff + 1, 'minutes');
			const dataTemperature = { time: startAddTempDiff.format('HH:mm') };
			console.log(startAddTempDiff.format('YYYY-MM-DD HH:mm'), dataTemperature);
			// if()
			tempDiff++;
		}
		// let startMinutes = moment(startDate).minutes() % 1;
		// let endMinutes = 5 - (moment(dateNow).get('minutes') % 1);
		// let startProcess = moment(startDate).subtract(startMinutes, 'minutes');
		// let endProcess = moment(dateNow).add(endMinutes, 'minutes');
		// let index = 0;
		// while (startProcess.unix() < endProcess.unix()) {
		// 	let nextProcess = moment(startProcess).add(1, 'minutes');
		// 	const dataTemperature = { time: startProcess.format('HH:mm') };
		// 	// const dataHumidity = { time: startProcess.format('HH:mm') };
		// 	for (const eachResult of data.data) {
		// 		console.log();
		// 		// console.log('eachResult', eachResult);
		// 		// console.log('eachResult.temperature', eachResult.temperature);
		// 		// console.log('eachResult.timestamp', eachResult.timestamp);
		// 		// console.log('startProcess.unix()', startProcess);
		// 		if (
		// 			eachResult.timestamp >= startProcess.unix() &&
		// 			eachResult.timestamp < nextProcess.unix()
		// 		) {
		// 			if (!!eachResult.temperature) {
		// 				console.log('test');
		// 				dataTemperature[`${eachResult.ref}_temperature`] =
		// 					eachResult.temperature;
		// 			}
		// 			if (!!eachResult.humidity) {
		// 				dataTemperature[`${eachResult.ref}_humidity`] = eachResult.humidity;
		// 			}
		// 		}
		// 	}
		// 	infoTemperature.push(dataTemperature);
		// 	startProcess = nextProcess;
		// }
	} catch (e) {
		console.log('error', e);
	}

	console.log('infoTemperature', infoTemperature);
	return infoTemperature;
};
// const main = async () => {
// 	// insert loop
// 	for (const eachRef of ref) {
// 		// loop each ref get API
// 		// push by ref
// 		// loop timestamp?
// 		let refDataPoint = [];

// 		try {
// 			const response = await axiosMainHelper.get(
// 				`devicePayload?deviceId=${deviceId}&ref=${eachRef}&timestampFrom=${timestampFrom}&timestampTo=${timestampTo}`
// 			);
// 			// console.log(response.data);
// 			// console.log(response);
// 			for (const eachData of response.data.data) {
// 				let dataPoint = {
// 					time: moment(eachData.timestamp).format(
// 						'dddd, MMMM Do, YYYY h:mm:ss A'
// 					),
// 					ref: eachData.ref,
// 					temperature: eachData.temperature,
// 					humidity: eachData.humidity,
// 				};
// 				// console.log(dataPoint);
// 				//   console.log(eachData.timestamp);
// 				//   console.log(eachData.timestamp);
// 				refDataPoint.push(dataPoint);
// 				console.log(refDataPoint);
// 			}
// 		} catch (e) {
// 			console.log(e);
// 		}
// 		pushRefToDataPoint.push();
// 	}
// };

main();
