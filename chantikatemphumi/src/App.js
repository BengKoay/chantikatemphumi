import moment, { unix } from 'moment';
import axios from 'axios';
// import aws4Interceptor from 'aws4-axios';
import React, { useState, useEffect, useRef } from 'react';
import { Line, Bar } from 'react-chartjs-2';
const axiosSolar = {
	baseURL: 'https://5l68pc71bd.execute-api.ap-southeast-1.amazonaws.com',
	authorization: 'a414f268-5ab2-4854-b4f7-97a9ea6d6fec',
};
const axiosMainHelper = axios.create({
	baseURL: axiosSolar.baseURL,
	headers: {
		Authorization: axiosSolar.authorization,
		'Access-Control-Allow-Origin': '*',
		'Access-Control-Allow-Headers': '*',
		'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
	},
});
const deviceId = 'b1d49ddf-6a7b-424d-b202-9b3715b5c74a';
// const ref = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
// const ref = [10];

const DynamicChart = () => {
	// const [chartData, setChartData] = useState({});
	const Chart = async () => {
		try {
			const res = await axiosMainHelper.get(
				`devicePayload?deviceId=${deviceId}&ref=10`
			);
			for (const row of res.data) {
				console.log(row);
			}
		} catch (e) {
			console.log('error ', e);
		}
	};
	useEffect(() => {
		Chart();
	}, []);

	return <div></div>;
};
export default DynamicChart;
